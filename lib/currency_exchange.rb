require 'json'
require 'yaml'
require 'date'

module CurrencyExchange

  # Return the exchange rate between from_currency and to_currency on date as a float.
  # Raises an exception if unable to calculate requested rate.
  # Raises an exception if there is no rate for the date provided.
  def self.rate(date, from_currency, to_currency)
    source = self.loadConfig('source');
    
    data = self.loadData(source);
    self.checkDate(date, data);
    
    ## Add base currency
    rates = data[date.to_s];
    rates[self.loadConfig('base_currency')] = 1.00;

    self.checkCurrencies(rates, from_currency, to_currency);
    
    return self.calculate(rates, from_currency, to_currency);
  end

  ## Simple load config function to allow for unit tests
  def self.loadConfig(setting)
    begin
      config = YAML.load_file(File.join(__dir__, 'config.yml'));

      return config[setting];
    rescue
      raise StandardError 'Could not load Config';
    end
  end

  ## Load file based on source, this method can be ammended to inlude other sources
  def self.loadData(source)
    begin
      case source
      when 'json'
        return self.loadJson();
      end
    rescue
      raise StandardError 'Could not load data';
    end
  end

  def self.checkDate(date, data)
    begin
      if data.key?(date.to_s) == false then raise "invalid date" end;
    rescue
      raise ArgumentError.new('No rates for date provide');
      return false;
    end
  end

  def self.loadJson
    file = File.read(File.join(__dir__, '../data/euro.json'));
    
    return JSON.parse(file);
  end

  def self.calculate(rates, from_currency, to_currency)
    begin
      from_rate = rates[from_currency];
      to_rate = rates[to_currency];

      return to_rate / from_rate;
    rescue
      raise StandardError.new('Could not calculate rate');
      return false;
    end
  end

  def self.checkCurrencies(rates, currency1, currency2)
   begin
      if rates.key?(currency1) == false then raise 'invalid currency' end;
      if rates.key?(currency2) == false then raise 'invalid currency' end;
    rescue
      raise StandardError.new('Currency not valid');
      return false;
    end
    return true;
  end

end
