# These are just suggested definitions to get you started.  Please feel
# free to make any changes at all as you see fit.


# http://test-unit.github.io/
require 'test/unit'
require 'currency_exchange'
require 'date'

class CurrencyExchangeTest < Test::Unit::TestCase
  def setup
  end

  def test_config_is_loading
    correct_source = "json";
    assert_equal correct_source, CurrencyExchange.loadConfig('source')
  end

  def test_loading_json
    test_rate = 1.1379;
    data = CurrencyExchange.loadJson();
    assert_equal test_rate, data['2018-12-11']['USD'];
  end

  def test_non_base_currency_exchange_is_correct
    correct_rate = 1.2870493690602498
    assert_equal correct_rate, CurrencyExchange.rate(Date.new(2018,11,22), "GBP", "USD")
  end

  def test_base_currency_exchange_is_correct
    correct_rate = 0.007763975155279502
    assert_equal correct_rate, CurrencyExchange.rate(Date.new(2018,11,22), "JPY", "EUR")
  end

  def test_invalid_date_throws_exeption
    assert_raise_message('No rates for date provide') {
      CurrencyExchange.rate(Date.new(2018,01,01), "JPY", "EUR")
    }
  end

  def test_invalid_currency_throws_exception
    assert_raise_message('Currency not valid') {
      CurrencyExchange.rate(Date.new(2018,11,22), "JPY", "PIZZA")
    }
  end
end
